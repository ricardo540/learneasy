Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  devise_for :users , path: '', path_names: { sign_in: 'login', sign_out: 'logout', password: 'password', 
            confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'singup' },
            :controllers => {:registrations => "registrations", :omniauth_callbacks => "omniauth_callbacks"}
  
  devise_scope :user do
    authenticated :user do
      root 'pages#home'
    end

    unauthenticated do
      root 'pages#landing', as: :unauthenticated_root
    end
  end

  get '/home'                   => 'pages#home'
  get '/edit_profile'           => 'users#edit'
  get '/beneficiaries'          => 'person_dependents#beneficiary'
  post '/dependent_destroy/:id' => 'person_dependents#destroy'
  post '/finance_data'          => 'persons#finance_data'
  get '/transaction/create'     => 'transactions#new'
  post '/transaction/create'    => 'transactions#create'
  
  resources :persons
  post '/users/password' => 'users#password'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
