function tasa_efectiva_mensual(tipo){
	var tea = get_tea(tipo);
	var tem = Math.pow( (1 + tea), 30/360) - 1;
	return tem;
}
function monthDiff(d1, d2) {
  var months;
  months = (d2.getFullYear() - d1.getFullYear()) * 12;
  months -= ( d1.getMonth() + 1);
  months += ( d2.getMonth() + 1);
  return months <= 0 ? 0 : months;
}
function valor_acumulado(retencion, tem, meses){
	acumulado = retencion * ( ( Math.pow(1 +  tem, meses) - 1 )/tem );
	return acumulado;
}
function valor_futuro(acumulado, tipo, dias_trasladar){
	var tea = get_tea(tipo);
	futuro = acumulado*Math.pow( (1 + tea), (dias_trasladar/360) );
	return futuro;
}
function get_tea(tipo){
	var tea = 0.05;
	switch(tipo){
		case 2: tea = 0.07; break;
		case 3: tea = 0.1; break;
		default: tea = 0.05;
	}
	return tea;
}