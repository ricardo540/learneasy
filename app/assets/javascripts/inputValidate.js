function validaPasswordIguales(password,confirm){
  if(password.val() != confirm.val()){
    confirm.focus().after( "<span class='error'>No coinciden las contraseñas</span>" );
    return false;
  }
  return true;
}

function TamañoTexto(input,longitud){
  if(input.val().length < longitud){
    input.focus().after( "<span class='error'>Minimo " + longitud + " caracteres</span>" );
    return false;
  }
  return true;
}

function validaEmail(email){
  console.log(email.attr('id'));
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    if(!emailreg.test(email.val())){
      email.focus();
      email.after("<span class='error'>Ingrese un email correcto (email@email.com)</span>"); 
      return false;     
    }
    return true;
}
function Invalid_char(input){
  var valid = /^[a-zA-Z0-9_\-@]+$/;
  if( input.val() == "" ){
      input.focus().after("<span class='error'>Campo requerido</span>"); 
      return false; 
  }else if(!valid.test(input.val())){
    input.focus().after("<span class='error'>No se permiten caracteres especiales ni espacios en blanco</span>"); 
    return false;     
  }
  return true;
}

function EfectoError(){
  setTimeout(function() {
        $(".error").fadeOut(500);
    },1000);
  return false;
}
function borrarError(e){
  if(e.keyCode == 13) 
    return false;
  $(".error").remove();
  return true;
}