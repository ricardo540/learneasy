$(document).ready(function(){
  $('#sign_in_user').on('submit',function(e){
    submitJson($(this),'/login','login success !!', 
                'Invalid user or password!!', '/home',e);
  });
  $('#register_users').on('submit',function(e){
    submitJson($(this),'/register','register success !!', 
                'the email or username is already in use!!', '/edit_profile',e);
  });
  $('#password_users').on('submit',function(e){
    submitJson($(this),'/password','success, please check you email', 
                      'Invalid email, try again!!', '',e);
  });
  $('#resend_form').on('submit',function(e){
    $('#submit-loader').removeClass('not-display');
    submitJson($(this),'/verification','success, please check you email', 
                       'Invalid email, try again!!', '',e);
  });
  $('#reset_pswd').on('submit',function(e){
    $('#submit-loader').removeClass('not-display');
    submitJson($(this),'/password','success, la contraseña se cambio correctamente', 
                      'email o token inválidos', '',e);
  });

});

function submitJson(form,urls,passed,errors,link,e){
  e.preventDefault();
  $.ajax({
    url: urls, cache: false,
    type: 'POST', data : form.serialize(),
    success: function(json) {
        submitHelperMessage('Notice',passed);
        if(link != ''){ location.href = link; }
        return false; //ends of instruction
    },
    error: function(request, status, error) {
        submitHelperMessage('Warning',errors);
    }
  });
}

function submitHelperMessage(titles,messages){
  $('#submit-loader').addClass('not-display');
  var type = "info";
  if(titles == 'Warning'){ type = "warning"; }
  $.notify({
    // options
    icon: 'fa fa-info-circle',
    message: messages
  },{
    // settings
    placement: {
        from: "bottom",
        align: "center"
    },
    type: type
  });
}