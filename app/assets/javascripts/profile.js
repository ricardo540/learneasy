function Profile(){
    $('.text-danger').remove();

    if(!TamañoTexto($('#user_first_name'),2)){
      return EfectoError();
    }else if( !TamañoTexto( $('#user_last_name'), 2 ) ){
      return EfectoError();
    }
    return true;
}
function setPasswordVal(){
    $('.text-danger').remove();

    if(!TamañoTexto($('#user_password_actual'),8)){
      return EfectoError();
    }else if( !TamañoTexto( $('#user_password') , 8 ) ){
      return EfectoError();
    }else if( !validaPasswordIguales( $("#user_password"),$('#user_password_confirmation')) ){
        return EfectoError();
    }
    return true;

}
function forgotPassword(){
    $('.text-danger').remove();
    if( !validaEmail($("#user_email")) ){
      return EfectoError();
    }else if( !TamañoTexto( $('#user_password') , 8 ) ){
      return EfectoError();
    }else if( !validaPasswordIguales( $("#user_password"),$('#user_password_confirmation')) ){
        return EfectoError();
    }
    return true; 
}