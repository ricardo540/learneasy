var login = true;
var form_click = false;

$(document).ready(function(){
  
  Landing_orientation();
    /* Mostrando los modals*/
  /* Fin login modals*/   
  $('#modals-container').click( set_form );
  $('.modals-container').click( set_form );
  $('.container-opacity').click(function(e){
    e.stopPropagation();
    if(form_click){
      form_click = false;
    }else{ $('.opened').click(); }
  });

	$(window).on('orientationchange', function(event) {
      setTimeout(Landing_orientation, 500);
  });

});

function auth_show(){
  $('#landing-auth-container').show();
  bounce_effect('#landing-auth-container #modals-container','bounceInDown');
  $('#user_email').focus();
}
function set_form(){
  form_click = true;
}
function box_close(input){
	input.parent().hide();
}
function Landing_orientation(){
  $('.max-height').css({ 'height' : window.innerHeight });
}
function bounce_effect(id,animation){
  $(id).addClass('animated ' + animation);
  window.setTimeout( function(){ $(id).removeClass('animated ' + animation); }, 750); 
}
function bounce_effect_delay(id,animation,time){
  $(id).addClass('animated ' + animation);
  window.setTimeout( function(){ $(id).removeClass('animated ' + animation); }, time); 
}