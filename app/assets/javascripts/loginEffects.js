function login_to_register(){
  $('#pass-container-form').hide();
  $('#reset-pswd-container-form').hide();
  bounce_effect_delay('#landing-auth-container #modals-container','bounceOut',550);
	setTimeout(function(){
		register_content();
		$('#login-container-form').hide();
		$('#registrations-container-form').show();
	  bounce_effect('#landing-auth-container #modals-container','bounceIn');
	}, 550);
	return true;
}
function register_to_Login(){
  $('#pass-container-form').hide();
  $('#reset-pswd-container-form').hide();
  bounce_effect_delay('#landing-auth-container #modals-container','bounceOut',550);
	setTimeout(function(){
		login_content();
		$('#registrations-container-form').hide();
		$('#login-container-form').show();
	  bounce_effect('#landing-auth-container #modals-container','bounceIn');
	}, 550);
	return true;
}
function login_to_password(){
  $('#registrations-container-form').hide();
  bounce_effect_delay('#landing-auth-container #modals-container','bounceOut',550);
	setTimeout(function(){
		$('#pass-container-form').show();
		$('#login-container-form').hide();
	  bounce_effect('#landing-auth-container #modals-container','bounceIn');
	}, 550);
	return true;
}
function password_to_login(){
  $('#registrations-container-form').hide();
  bounce_effect_delay('#landing-auth-container #modals-container','bounceOut',550);
	setTimeout(function(){
		$('#pass-container-form').hide();
		$('#login-container-form').show();
	  bounce_effect('#landing-auth-container #modals-container','bounceIn');
	}, 550);

	return true;
}
function reset_password_to_login(){
  login = false;
	$('#reset-pswd-container-form').slideDown('slow');
	$('#login-container-form').slideUp("slow" );
	return true;
}
function reset_back_to_login(){
	$('#reset-pswd-container-form').slideUp('slow');
	$('#login-container-form').slideDown("slow" );
  return true;
}
function login_content(){
	$('#auth-image').addClass('welcome-back');
	$('.auth-content').removeClass('not-display');
	$('.register-content').addClass('not-display');
}
function register_content(){
	$('#auth-image').removeClass('welcome-back');
	$('.auth-content').addClass('not-display');
	$('.register-content').removeClass('not-display');
}