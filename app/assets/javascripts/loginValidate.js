function validaLogin(){
  $('.error').remove();
  if( !validaEmail($("#sign_in_user #user_email")) ){
      return EfectoError();
  }else if( !TamañoTexto( $('#sign_in_user #user_password'), 8 )){
      return EfectoError();
  }
  $('#submit-loader').removeClass('not-display');
  return true;
}
function validaRegistro(){
    $('.error').remove();
    if( !validaEmail( $(".RegEmail") ) ) {
        return EfectoError();
    }else if( !TamañoTexto( $('.RegPassword'), 8 ) ){
        return EfectoError();
    }else if( !TamañoTexto( $('.RegConfirmPassword'), 8 ) ){
        return EfectoError();
    }else if( !validaPasswordIguales($('.RegPassword'), $('.RegConfirmPassword')) ){
        return EfectoError();
    }
    $('#submit-loader').removeClass('not-display');
    return true;
}
function valid_email(){
  $('.error').remove();
  if( !validaEmail( $(".confirm_email_user") ) ) {
    return EfectoError();
  }
  return true;
}
function validaRegistro2(){
    $('.error').remove();
    if( !Invalid_char( $('.RegUsername2') ) ){
        return EfectoError();
    }else if( !TamañoTexto( $(".RegUsername2"), 4 ) ) {
        return EfectoError();
    }else if( !validaEmail( $(".RegEmail2") ) ) {
        return EfectoError();
    }else if( !TamañoTexto( $('.RegPassword2'), 8 ) ){
        return EfectoError();
    }
    $('#submit-loader').removeClass('not-display');
    return true;
}
function validaOlvidoPassword(){
    $('.error').remove();
    if( !validaEmail($(".lostEmail")) )
      return EfectoError();
    $('#submit-loader').removeClass('not-display');
    return true;
}
function reset_passwords(){
    $('.text-danger').remove();

    if(!validaEmail($('.restore-password'))){
      return EfectoError();
    }else if( !TamañoTexto( $('#reset_pswd #user_password') , 8 ) ){
      return EfectoError();
    }else if( !validaPasswordIguales( $("#reset_pswd #user_password"),$('#reset_pswd #user_password_confirmation')) ){
        return EfectoError();
    }
    return true;

}