module UsersHelper

	def create_person(id, fbGraph, img)
		person = Person.create(:first_name => fbGraph.first_name, 
			:last_name => fbGraph.last_name, :user_id => id, :picture => img )
		if fbGraph.birthday.present?
			person.birth_date = fbGraph.birthday
		end

		if fbGraph.gender.present?
			person.gender = fbGraph.gender
		end 

		person.save
		person
	end

end
