class PersonDependent < ActiveRecord::Base
	belongs_to :user

	def person
		Person.find_by(:id => person_id)
	end

end