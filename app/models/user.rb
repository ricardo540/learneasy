class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers =>  [:facebook]

  has_many :dependents, foreign_key: "user_id", class_name: "PersonDependent"
  has_one  :person

  def self.from_omniauth(fbgraph)
    user = where("email = ?", fbgraph.email).first
    user
  end
  
  def self.from_omniauth_register(fbgraph, pswd)
    user = User.new
    user.email = fbgraph.email
    user.password = pswd
    user
  end

  def financial
    FinancialData.find_by(:person_id => person_id)
  end

  def to_retire
    person.age_retirement - person.age
  end

  def _ready
    return person.age_retirement.present? && person.age.present? &&
          financial.salary.present? && person.birth_date.present?
  end

end
