class Person < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :user
  belongs_to :person_dependent
  has_many :transaction_balances

  def profile_picture(options = { size: 80, klass: 'img-circle' })
    size = options[:size]
    klass = options[:klass]
    if picture.present?
      helpers.image_tag(
                  picture + "?height=#{size}&width=#{size}", 
                  class: "img-circle #{klass}", 
                  style: "max-width: #{size}px;")
    else
      helpers.image_tag(
                  "default_user.jpg", 
                  class: "img-circle #{klass}", 
                  style: "max-width: #{size}px;")
    end
  end

  def full_name
  	if first_name.present? && last_name.present?
  		first_name.split(' ')[0] + ' ' + last_name.split(' ')[0]
  	elsif first_name.present?
  		first_name
  	else
  		User.find_by(:person_id => id).email
  	end
  end

  def short_name
    if first_name.present?
      first_name.split(' ')[0]
    else
      User.find_by(:person_id => id).email.split('@')[0]
    end
  end
  
  private
    def helpers
      ActionController::Base.helpers
    end

end
