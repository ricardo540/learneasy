class TransactionBalance < ActiveRecord::Base
  belongs_to :person

	def details
		TransactionBalanceDetail.where(:transaction_balance_id => id)
	end
end