class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include UsersHelper
  
  
  #Loguenado con facebook
	def facebook
	  auth = request.env["omniauth.auth"]
    fbGraph = FbGraph2::User.me(request.env["omniauth.auth"].credentials.token).fetch(:fields => 'first_name,last_name,email,birthday,gender')
    sing_up_facebook( fbGraph, auth.info.image )

	end

  def sign_in_fb(user, url)
    
    flash[:info] = 'Registro con Facebook exitosamente!'
    sign_in user, :bypass => true
    redirect_to url

  end
  
  def sing_up_facebook(fbGraph, img) #sing up or sing in
    user = User.from_omniauth(fbGraph)
    if user.present? #is already register
      if !user.person.picture.present?
        user.person.update_attribute(:picture, img)
      end
      sign_in_fb(user, root_path)
    else
      fb_routine(fbGraph, img)
    end
  end

  private

  def fb_routine(fbGraph, img)

    if fbGraph.email.present?
        password = Devise.friendly_token[0,20]
        user = User.from_omniauth_register(fbGraph, password)
        if user.save
          person = create_person( user.id, fbGraph, img)
          user.update_attribute(:person_id, person.id)
          #UserMailer.send_password(user,'uDocz: envio de contraseña', password).deliver_now
          sign_in_fb(user, '/edit_profile')
        else
          flash[:warning] = 'Error no se pudo comprobar tus datos'
          redirect_to root_path
        end
    else
        flash[:warning] = 'Error no se pudo comprobar tus datos'
        redirect_to root_path
    end

  end

end