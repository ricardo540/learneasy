class UsersController < ApplicationController

  def edit
    if current_user.present?
      @view = '#user-li'
      @person = current_user.person
      @finance = FinancialData.find_by(:person_id => @person.id) || FinancialData.new
    else
      flash[:notice] = "El usuario no existe"
      redirect_to root_path
    end
  end


  def update
	  flash[:notice] = "Se actualizo tu perfil"	   
    redirect_to '/edit_profile'
  end

  def destroy
  end

  def password
    @user = current_user
    if @user.valid_password?(params[:user][:password_actual])
        if @user.update_attributes(user_password)
          sign_in @user, :bypass => true
          flash[:notice] = "Se actualizó tu contraseña" 
        else       
          flash[:danger] = "Completa los campos" 
        end
    else
      flash[:danger] = "La contraseña actual no es válida"
    end
    redirect_to '/edit_profile'
  end

  private

    
  def user_password
    params.require(:user).permit(:password, :password_confirmation)
  end

end