class PersonsController < ApplicationController

  def show
  end
  
  def edit
  end

  def create
    person = Person.create( user_params )

    if params[:person][:birth_date].present?
      year = Date.today.year - params[:person][:birth_date].to_date.year
      person.age = year
    end

    if person.save
      PersonDependent.create(:dependent_type => params[:parent],
        :person_id => person.id, :user_id => current_user.id)
      flash[:notice] = "Se agregó correctamente" 
    else
      flash[:danger] = "No se pudo agregar"  
    end
    redirect_to '/beneficiaries'
  end
  
  def update

    @person = Person.find_by(:id => current_user.person_id) 
    if @person.update_attributes(user_params)
      if params[:person][:birth_date].present?
        year = Date.today.year - params[:person][:birth_date].to_date.year
        @person.age = year
        @person.save
      end
	    flash[:notice] = "Se actualizo tu perfil"	
      redirect_to :back
    else       
	    flash[:danger] = "No se pudo actualizar"	
    redirect_to '/edit_profile'
    end 

  end

  def destroy
  end

  def finance_data
    finance = FinancialData.find_by(:person_id => current_user.person.id)
    if finance.present?
      finance.update_attributes( finance_params )
    else
      finance = FinancialData.create( finance_params )
      finance.person_id = current_user.person.id
      finance.save
    end
    flash[:notice] = 'se guardo correctamente'
    redirect_to :back
  end
  
  private
  
  def user_params
    params.require(:person).permit(:first_name, :last_name, :birth_date, :gender, 
      :dni, :age_retirement)
  end

  def finance_params
    params.require(:financial_data).permit(:person_id, :salary, :initial_balance, :afp_id, :wished_input)
  end
  

end