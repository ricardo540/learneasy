class RegistrationsController < Devise::RegistrationsController

	def new
    super
  end

  def create
    build_resource(sign_up_params)
		user = User.find_by(:email => params[:user][:email])
		
		if user.present?
			clean_up_passwords resource	      
			render :json => { 'errors' => { 'email' => 'El email esta en uso' }}.to_json, :status => 401
		else
			user = User.create( user_params )
			if user.save
				if user.active_for_authentication?
					sign_up(resource_name, user)
				else
					expire_data_after_sign_in!
				end
				person = Person.create(:user_id => user.id)
				user.person_id = person.id
				user.save
				render :json => { 'message' => { 'user' => 'se creó exitosamente' }}.to_json, :status => 202
			else
				clean_up_passwords resource	      
				render :json => { 'errors' => { 'email' => 'El email esta en uso' }}.to_json, :status => 401
			end
		end

  end

  def update
    super
  end

  private

  def user_params
		params.require(:user).permit(:email, :password,:password_confirmation)
	end
	
end