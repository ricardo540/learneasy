class TransactionsController < ApplicationController
  
  def new
  	if current_user._ready
  		@transaction = TransactionBalance.new
  	else
  		flash[:warning] = 'Ingresa tus datos financieros para poder continuar'
  		redirect_to '/edit_profile'
  	end
  end

  def create
  	@transaction = TransactionBalance.find_by(:id => params[:transaction_id])
  	if @transaction.present?
  		@transaction.update_attribute(:result, params[:result])
  	else
  		_create_transaction
  	end
    
  	TransactionBalanceDetail.create(:salary => params[:transaction][:salary],
  		:result => params[:result], :start_date => params[:date],
  		:type_fund_id => params[:transaction][:type_fund_id], :months => params[:months],
  		:transaction_id => @transaction.id, :end_date => params[:birth_date])
		
		respond_to do |format|
			format.js
			format.html
		end
  end

  private

  def _create_transaction
  		@transaction = TransactionBalance.create(:person_id => params[:transaction][:person_id],
  			:transaction_date => Date.today, :result => params[:result])
  end

end