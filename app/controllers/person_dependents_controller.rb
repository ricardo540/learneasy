class PersonDependentsController < ApplicationController

  def beneficiary
  	@persons = current_user.dependents
  end

  def destroy
  	dependent = PersonDependent.find_by(:id => params[:id])
  	Person.find_by(:id => dependent.person_id).destroy
  	dependent.destroy
  	flash[:notice] = 'Se eliminó correctamente'
  	redirect_to :back
  end

end