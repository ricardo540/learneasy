class PagesController < ApplicationController
  def home
  	@transactions = current_user.person.transaction_balances
  end

  def landing
  end

end
