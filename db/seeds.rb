# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#Afp.create(:name => 'AFP Integtra', :short_name => 'AFPI', :insurance => 0.133, :commission_balance => 0.123, :comission_input => 0.120)
#Afp.create(:name => 'AFP Prima', :short_name => 'AFPP', :insurance => 0.133, :commission_balance => 0.119, :comission_input => 0.125)
#Afp.create(:name => 'Profuturo', :short_name => 'PRO', :insurance => 0.133, :commission_balance => 0.146, :comission_input => 0.120)

FundType.create(year: 2016, month: 10, type_index: 1, performance: 0.03)
FundType.create(year: 2016, month: 10, type_index: 2, performance: 0.05)
FundType.create(year: 2016, month: 10, type_index: 3, performance: 0.09)
