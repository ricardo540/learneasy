# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161124131211) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "afps", force: :cascade do |t|
    t.string  "name"
    t.string  "short_name"
    t.decimal "comission_input"
    t.decimal "commission_balance"
    t.decimal "insurance"
  end

  create_table "financial_data", force: :cascade do |t|
    t.integer "person_id"
    t.decimal "salary"
    t.decimal "initial_balance"
    t.decimal "wished_input"
    t.integer "afp_id"
  end

  create_table "fund_types", force: :cascade do |t|
    t.integer  "year"
    t.integer  "month"
    t.decimal  "performance"
    t.integer  "type_index"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "dni"
    t.date     "birth_date"
    t.integer  "age"
    t.string   "gender"
    t.integer  "age_retirement"
    t.string   "picture"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "person_dependents", force: :cascade do |t|
    t.string  "dependent_type"
    t.integer "person_id"
    t.integer "user_id"
  end

  create_table "transaction_balance_details", force: :cascade do |t|
    t.integer "afp_id"
    t.integer "year"
    t.decimal "yearly_input"
    t.decimal "current_balance"
    t.decimal "salary"
    t.integer "transaction_balance_id"
  end

  create_table "transaction_balances", force: :cascade do |t|
    t.decimal  "result"
    t.integer  "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.integer  "person_id"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
