class CreateFundTypes < ActiveRecord::Migration
  def change
    create_table :fund_types do |t|
    	t.integer :year
    	t.integer :month
    	t.decimal :performance
    	t.integer :type_index
      t.timestamps null: false
    end
  end
end
