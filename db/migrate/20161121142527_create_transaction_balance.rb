class CreateTransactionBalance < ActiveRecord::Migration
  def change
    create_table :transaction_balances do |t|
    	t.decimal	:result
    	t.integer	:person_id
    	
      t.timestamps null: false
    end
  end
end
