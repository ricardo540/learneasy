class CreatePerson < ActiveRecord::Migration
  def change
    create_table :people do |t|
    	t.string   :first_name
    	t.string   :last_name
    	t.string   :dni
    	t.date     :birth_date
    	t.integer  :age
    	t.string   :gender
    	t.integer  :age_retirement
    	t.string   :picture
      t.integer  :user_id
    	t.timestamps
    end
  end
end
