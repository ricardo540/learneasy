class CreateTransactionBalanceDetail < ActiveRecord::Migration
  def change
    create_table :transaction_balance_details do |t|
    	t.integer     :afp_id
	    t.integer     :year
	    t.decimal :yearly_input
	    t.decimal :current_balance
	    t.decimal :salary
	    t.integer     :transaction_balance_id
    end
  end
end
