class CreateFinancialData < ActiveRecord::Migration
  def change
    create_table :financial_data do |t|
    	t.integer :person_id
    	t.decimal :salary
    	t.decimal :initial_balance
    	t.decimal	:wished_input
    	t.integer :afp_id
    end
  end
end
