class CreatePersonsDependents < ActiveRecord::Migration
  def change
    create_table :person_dependents do |t|
    	t.string 	:dependent_type
    	t.integer	:person_id
    	t.integer	:user_id
    end
  end
end
