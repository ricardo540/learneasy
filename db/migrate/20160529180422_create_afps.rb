class CreateAfps < ActiveRecord::Migration
  def change
    create_table :afps do |t|
    	t.string 	:name
    	t.string 	:short_name
    	t.decimal	:comission_input
    	t.decimal :commission_balance
      t.decimal :insurance
    end
  end
end
